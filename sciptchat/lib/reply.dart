import 'package:flutter/material.dart';
import 'package:sciptchat/kategori.dart';
// ignore: unused_import
// ignore: unused_import
import 'package:sciptchat/editscript.dart';
// ignore: unused_import
import 'package:sciptchat/scriptchat.dart';
import 'package:sciptchat/hasilscript_bookmark.dart';
import 'package:sciptchat/hasilscript.dart';
import 'package:sciptchat/problem.dart';
// ignore: unused_import
import 'package:flutter_slidable/flutter_slidable.dart';

void main() => runApp(const reply());

// ignore: camel_case_types
class reply extends StatelessWidget {
  const reply({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text('Hasil', style: TextStyle(color: Colors.black)),
        centerTitle: true,
        iconTheme: const IconThemeData(
          color: Color(0XFF00AEFF), //change your color here
        ),
      ),
      body: const MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  // final List<int> numbers = [1, 2, 3, 5, 8, 13, 21, 34, 55];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      color: Colors.white,
      child: ListView(
          padding: const EdgeInsets.only(right: 16, left: 16, top: 12),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    // ignore: sized_box_for_whitespace
                    child: Container(
                      width: 40,
                      // color: Colors.amber,
                      child: Image.asset(
                        "assets/Group165.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 3.0,
                    ),
                  ),
                  Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                            color: const Color(0XFFF8F9FE),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          height: 40,
                          child: const TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              prefixIcon: Icon(Icons.search),
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 14, top: 11, right: 15),
                              hintText: 'Search',
                            ),

                            // Image.asset("assets/"),
                          ))),
                ],
              ),
            ),
            Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 16, horizontal: 0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // ignore: sized_box_for_whitespace
                              Container(
                                width: 220,
                                // color: Colors.red,
                                child: const Text(
                                  "Erigo Script",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Color(0XFF464E5F),
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              const SizedBox(height: 8),
                              // ignore: sized_box_for_whitespace
                              Container(
                                  width: 220,
                                  // color: Colors.red,
                                  child: const Text(
                                      "Template script terbaik untuk script anda",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: Color(0XFF9F9FB9)))),
                            ],
                          )),
                      const SizedBox(
                          // height: 8,
                          width: 0),
                      Align(
                          alignment: Alignment.centerRight,
                          // Padding(
                          // padding: const EdgeInsets.symmetric(horizontal: 5.0),
                          child: Container(
                              // color: Colors.red,
                              padding: const EdgeInsets.all(4),
                              child: TextButton(
                                onPressed: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return const kategori();
                                      },
                                    ),
                                  );
                                },
                                child: const Text("Edit"),
                              )))
                    ])),
            // SizedBox(height: 16),
            Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
                height: 70,
                child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        child: Container(
                          width: 40,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                color: const Color(0XFFD2C9C5),
                                width: 1,
                              )),
                          // padding: EdgeInsets.all(15),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const bookmark();
                                  },
                                ),
                              );
                            },
                            child: const Center(
                                child: Icon(
                              Icons.bookmark,
                              color: Color(0XFF9F9FB9),
                            )),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(
                                color: const Color(0XFFD2C9C5),
                                width: 1,
                              )),
                          // padding: EdgeInsets.all(15),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const hasil();
                                  },
                                ),
                              );
                            },
                            child: const Center(
                              child: Text('GREETING',
                                  style: TextStyle(color: Color(0XFFB5B5C3))),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        child: Container(
                          width: 145,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            // border: Border.all(
                            //   color: Color(0XFFD2C9C5),
                            //   width: 1,
                            // )
                          ),
                          // padding: EdgeInsets.all(15),
                          child: ElevatedButton(
                              onPressed: () {},
                              child: const Center(
                                child: Text(
                                  'REPLY GREETING',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              style: ElevatedButton.styleFrom(
                                backgroundColor: const Color(0XFF00AEFF),
                              )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4.0),
                        child: Container(
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(
                                color: const Color(0XFFD2C9C5),
                                width: 1,
                              )),

                          // padding: EdgeInsets.all(15),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const problem();
                                  },
                                ),
                              );
                            },
                            child: const Center(
                              child: Text(
                                'ASK PROBLEM',
                                style: TextStyle(color: Color(0XFFB5B5C3)),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ])),
          ]),
    ));
  }
}
