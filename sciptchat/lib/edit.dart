import 'package:flutter/material.dart';
import 'package:sciptchat/kategori.dart';
import 'package:sciptchat/scriptchat.dart';
import 'package:flutter_switch/flutter_switch.dart';

// import 'package:cool_alert/cool_alert.dart';
// import 'package:rflutter_alert/rflutter_alert.dart';

void main() => runApp(const edit());

const String _heroSavePopup = 'save-popup-hero';

// ignore: camel_case_types
class edit extends StatelessWidget {
  const edit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title:
            const Text('Tambah Script', style: TextStyle(color: Colors.black)),
        centerTitle: true,
        iconTheme: const IconThemeData(
          color: Color(0XFF00AEFF), //change your color here
        ),
      ),
      body: const MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final _formKey = GlobalKey<FormState>();
  String? gender;
  String? pay;
  //no radio button will be selected
  bool isSwitched = false;
  bool status = false;
  // Initial Selected Value
  String dropdownvalue = ' Fashion Pria';

  // List of items in our dropdown menu
  var items = [
    ' Fashion Pria',
    ' Fashion Wanita',
    ' Makanan',
    ' Peralatan Rumah Tangga',
    ' Fashion Muslim',
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView(
          padding: const EdgeInsets.only(right: 20, left: 20, top: 12),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 15),
                    width: 85,
                    height: 67,
                    // color: Colors.amber,
                    child: Row(children: [
                      GestureDetector(
                        onTap: () {},
                        child: Image.asset(
                          'assets/Step1(3).png',
                          fit: BoxFit.cover,
                        ),
                      )
                    ]),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 15),
                    width: 25,
                    height: 67,
                    // color: Colors.amber,
                    child: Row(children: [
                      Image.asset(
                        'assets/Arrow.png',
                        fit: BoxFit.cover,
                      )
                    ]),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 15),
                    width: 100,
                    height: 67,
                    // color: Colors.amber,
                    child: Row(children: [
                      GestureDetector(
                        onTap: () {},
                        child: Image.asset(
                          'assets/Step5(3).png',
                          // width: 85,
                          // height: 65,
                          fit: BoxFit.cover,
                        ),
                      )
                    ]),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 25),
            Column(
              children: [
                // ignore: avoid_unnecessary_containers
                Container(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        constraints:
                            const BoxConstraints(minWidth: 333, maxWidth: 900),
                        child: const Text('Foto Brand Produk (Opsional)',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700))),
                    const SizedBox(height: 15),
                    Container(
                      // color: Colors.red,
                      padding: const EdgeInsets.only(top: 4),
                      // width: 25,
                      height: 67,
                      constraints:
                          const BoxConstraints(minWidth: 333, maxWidth: 900),
                      child: Image.asset(
                        "assets/avataredit.png",
                        alignment: Alignment.centerLeft,
                      ),
                    ),
                    const SizedBox(height: 15),
                    Container(
                        constraints:
                            const BoxConstraints(minWidth: 333, maxWidth: 900),
                        child: const Text(
                          'Allowed file types: png, jpg, jpeg',
                          style: TextStyle(
                            color: Color(0XFF71727A),
                            fontSize: 12,
                          ),
                        ))
                  ],
                )),
                const SizedBox(height: 25),
                Row(
                  children: [
                    // ignore: avoid_unnecessary_containers
                    Container(
                        child: const Text('Kategori',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700))),
                    // ignore: avoid_unnecessary_containers
                    Container(
                        child: const Text('*',
                            style: TextStyle(
                                color: Color(0XFFFF616D),
                                fontSize: 16,
                                fontWeight: FontWeight.w700)))
                  ],
                ),
                const SizedBox(height: 12),
                // ignore: sized_box_for_whitespace
                Container(
                  height: 75,
                  child: TextField(
                    enabled: false,
                    decoration: InputDecoration(
                        hintText: "Fashion Pria",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        )),
                  ),
                ),
                const SizedBox(height: 4),
                Form(
                  key: _formKey,
                  child: Container(
                      padding: const EdgeInsets.all(2.0),
                      child: Column(children: [
                        Padding(
                            padding: const EdgeInsets.all(0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                    constraints: const BoxConstraints(
                                        minWidth: 333, maxWidth: 900),
                                    child: Row(
                                      children: [
                                        // ignore: avoid_unnecessary_containers
                                        Container(
                                            child: const Text('Nama Script',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w700))),
                                        // ignore: avoid_unnecessary_containers
                                        Container(
                                            child: const Text('*',
                                                style: TextStyle(
                                                    color: Color(0XFFFF616D),
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w700)))
                                      ],
                                    )),
                                const SizedBox(height: 12),
                                TextFormField(
                                  decoration: InputDecoration(
                                      hintText: "Nama Script",
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(12.0),
                                      )),
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Silahkan masukkan nama script terlebih dahulu ';
                                    }
                                    return null;
                                  },
                                ),
                              ],
                            )),
                        const SizedBox(height: 20),
                        Container(
                            constraints: const BoxConstraints(
                                minWidth: 333, maxWidth: 900),
                            child: Row(
                              children: [
                                // ignore: avoid_unnecessary_containers
                                Container(
                                    child: const Text('Nama Produk',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w700))),
                                // ignore: avoid_unnecessary_containers
                                Container(
                                    child: const Text('*',
                                        style: TextStyle(
                                            color: Color(0XFFFF616D),
                                            fontSize: 16,
                                            fontWeight: FontWeight.w700)))
                              ],
                            )),
                        const SizedBox(height: 12),
                        TextFormField(
                          decoration: InputDecoration(
                              hintText: "Nama Produk",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12.0),
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Silahkan masukkan nama produk terlebih dahulu ';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 20),
                        Padding(
                            padding: const EdgeInsets.all(0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                    constraints: const BoxConstraints(
                                        minWidth: 333, maxWidth: 900),
                                    child: Row(
                                      children: [
                                        // ignore: avoid_unnecessary_containers
                                        Container(
                                            child: const Text('Harga',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w700))),
                                        // ignore: avoid_unnecessary_containers
                                        Container(
                                            child: const Text('*',
                                                style: TextStyle(
                                                    color: Color(0XFFFF616D),
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w700)))
                                      ],
                                    )),
                                const SizedBox(height: 12),
                                TextFormField(
                                  decoration: InputDecoration(
                                      hintText: "Rp Masukkan Harga",
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(12.0),
                                      )),
                                  keyboardType: TextInputType.number,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Silahkan masukkan harga terlebih dahulu ';
                                    }
                                    return null;
                                  },
                                ),
                              ],
                            ))
                      ])),
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    // ignore: avoid_unnecessary_containers
                    Container(
                        child: const Text('Target Market',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700))),
                    // ignore: avoid_unnecessary_containers
                    Container(
                        child: const Text('*',
                            style: TextStyle(
                                color: Color(0XFFFF616D),
                                fontSize: 16,
                                fontWeight: FontWeight.w700)))
                  ],
                ),
                const SizedBox(height: 8),
                // ignore: avoid_unnecessary_containers
                Container(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // ignore: avoid_unnecessary_containers
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                // Padding(
                                //   padding: const EdgeInsets.only(right: 8.0),
                                // child:
                                Radio(
                                  value: "lakilaki",
                                  groupValue: gender,
                                  onChanged: (value) {
                                    setState(() {
                                      gender = value.toString();
                                    });
                                  },
                                ),
                                // ),
                                Container(
                                  padding: const EdgeInsets.only(
                                      top: 20, right: 0, left: 0, bottom: 20),
                                  child: const Text('Laki-Laki',
                                      style: TextStyle(
                                          color: Color(0XFF464E5F),
                                          fontSize: 14)),
                                )
                              ],
                            ),
                          ),
                          // SizedBox(width: 4),
                          Row(
                            children: [
                              Radio(
                                value: "perempuan",
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value.toString();
                                  });
                                },
                              ),
                              const Text('Perempuan',
                                  style: TextStyle(
                                      color: Color(0XFF464E5F), fontSize: 14)),
                            ],
                          ),
                          // SizedBox(width: 8),
                          Row(
                            children: [
                              Radio(
                                value: "semua",
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value.toString();
                                  });
                                },
                              ),
                              const Text('Semua',
                                  style: TextStyle(
                                      color: Color(0XFF464E5F), fontSize: 14))
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const Divider(
                  height: 5,
                  thickness: 1,
                  indent: 15,
                  endIndent: 0,
                  color: Color(0XFFE8EDF0),
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    // ignore: avoid_unnecessary_containers
                    Container(
                        child: const Text('Jenis Pembayaran',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700))),
                    // ignore: avoid_unnecessary_containers
                    Container(
                        child: const Text('*',
                            style: TextStyle(
                                color: Color(0XFFFF616D),
                                fontSize: 16,
                                fontWeight: FontWeight.w700)))
                  ],
                ),
                const SizedBox(height: 8),
                // ignore: avoid_unnecessary_containers
                Container(
                  child: Column(
                    children: [
                      // Padding(
                      //   padding: const EdgeInsets.symmetric(horizontal: 20),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Radio(
                                value: "transfer",
                                groupValue: pay,
                                onChanged: (value) {
                                  setState(() {
                                    pay = value.toString();
                                  });
                                },
                              ),
                              const Text('Transfer Bank',
                                  style: TextStyle(
                                      color: Color(0XFF464E5F), fontSize: 14))
                            ],
                          ),
                          const SizedBox(width: 25),
                          Row(
                            children: [
                              Radio(
                                value: "cod",
                                groupValue: pay,
                                onChanged: (value) {
                                  setState(() {
                                    pay = value.toString();
                                  });
                                },
                              ),
                              const Text('COD',
                                  style: TextStyle(
                                      color: Color(0XFF464E5F), fontSize: 14))
                            ],
                          ),
                        ],
                      ),
                      // ),
                    ],
                  ),
                ),
                const Divider(
                  height: 5,
                  thickness: 1,
                  indent: 15,
                  endIndent: 0,
                  color: Color(0XFFE8EDF0),
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    // ignore: avoid_unnecessary_containers
                    Container(
                        child: const Text('Izinkan CS Melihat Script ini?',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700))),
                    // ignore: avoid_unnecessary_containers
                    Container(
                        child: const Text('*',
                            style: TextStyle(
                                color: Color(0XFFFF616D),
                                fontSize: 16,
                                fontWeight: FontWeight.w700)))
                  ],
                ),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  // ignore: avoid_unnecessary_containers
                  child: Container(
                    // color: Colors.black,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        FlutterSwitch(
                          width: 75.0,
                          height: 35.0,
                          valueFontSize: 25.0,
                          toggleSize: 45.0,
                          value: status,
                          borderRadius: 30.0,
                          padding: 8.0,

                          // showOnOff: true,
                          onToggle: (val) {
                            setState(() {
                              status = val;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 12),
                const Divider(
                  height: 5,
                  thickness: 1,
                  indent: 10,
                  endIndent: 10,
                  color: Color(0XFFE8EDF0),
                ),
                const SizedBox(height: 20),
                Container(
                    padding: const EdgeInsets.only(
                        top: 20, right: 0, left: 0, bottom: 20),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(12)),
                    width: 300,
                    child: ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                // SizedBox(height:28),
                                return Container(
                                  color: Colors.transparent,
                                  // color: Colors.white,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          right: 28.0,
                                          left: 28.0,
                                          top: 240.0,
                                        ),
                                        // ignore: avoid_unnecessary_containers
                                        child: Container(
                                          // color: Colors.amber,

                                          child: Hero(
                                            tag: _heroSavePopup,
                                            createRectTween: (begin, end) {
                                              return CustomRectTween(
                                                  begin: begin!, end: end!);
                                            },
                                            child: Material(
                                              color: Colors.white,
                                              elevation: 0,
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: SingleChildScrollView(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          35, 15, 35, 15),
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    children: [
                                                      Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .only(
                                                                  bottom: 10),
                                                          child: Image.asset(
                                                              "assets/Group.png")
                                                          // size: 80,
                                                          // color: Color.fromARGB(
                                                          //     254, 99, 237, 122),
                                                          ),
                                                      const Text(
                                                        'Apakah anda yakin menyimpan data ini?',
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                      Container(
                                                        margin: const EdgeInsets
                                                                .fromLTRB(
                                                            0, 7, 0, 20),
                                                        child: const Text(
                                                          'Pastikan Tambah Script anda sudah benar',
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'Poppins',
                                                            fontSize: 12,
                                                            color: Color(
                                                                0XFF71727A),
                                                          ),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: double.infinity,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            GestureDetector(
                                                                onTap: () {
                                                                  showDialog(
                                                                      context:
                                                                          context,
                                                                      builder:
                                                                          (context) {
                                                                        return Container(
                                                                            color:
                                                                                Colors.transparent,
                                                                            // ignore: prefer_const_literals_to_create_immutables
                                                                            child: Column(children: [
                                                                              const AlertDialog(
                                                                                title: Text(
                                                                                  'Delete',
                                                                                  style: TextStyle(
                                                                                      // color: Color(0XFF71727A),
                                                                                      fontWeight: FontWeight.bold),
                                                                                  textAlign: TextAlign.center,
                                                                                ),
                                                                                content: Text(
                                                                                  'Apakah anda yakin ingin menghapus Script ini?',
                                                                                  style: TextStyle(color: Color(0XFF71727A), fontSize: 12),
                                                                                  textAlign: TextAlign.center,
                                                                                ),
                                                                              )
                                                                            ]));
                                                                      });
                                                                },
                                                                child:
                                                                    ElevatedButton(
                                                                  onPressed:
                                                                      () {
                                                                    showDialog(
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (context) {
                                                                          return Container(
                                                                              color: Colors.transparent,
                                                                              child: Column(children: [
                                                                                Padding(
                                                                                  padding: const EdgeInsets.only(
                                                                                    right: 28.0,
                                                                                    left: 28.0,
                                                                                    top: 240.0,
                                                                                  ),
                                                                                  // ignore: avoid_unnecessary_containers
                                                                                  child: Container(
                                                                                      child: Hero(
                                                                                          tag: _heroSavePopup,
                                                                                          createRectTween: (begin, end) {
                                                                                            return CustomRectTween(begin: begin!, end: end!);
                                                                                          },
                                                                                          child: Material(
                                                                                              color: Colors.white,
                                                                                              elevation: 0,
                                                                                              shape: RoundedRectangleBorder(
                                                                                                borderRadius: BorderRadius.circular(20),
                                                                                              ),
                                                                                              child: SingleChildScrollView(
                                                                                                  child: Padding(
                                                                                                      padding: const EdgeInsets.fromLTRB(35, 15, 35, 15),
                                                                                                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                                                                                                        Container(margin: const EdgeInsets.only(bottom: 10), child: Image.asset("assets/clarity_success-standard-line.png")),
                                                                                                        const Text(
                                                                                                          'Script Berhasil Dibuat',
                                                                                                          style: TextStyle(
                                                                                                            fontWeight: FontWeight.bold,
                                                                                                          ),
                                                                                                          textAlign: TextAlign.center,
                                                                                                        ),
                                                                                                        Container(
                                                                                                          margin: const EdgeInsets.fromLTRB(0, 7, 0, 20),
                                                                                                          child: const Text(
                                                                                                            'Anda dapat melihat script ini di halaman detail Script.',
                                                                                                            style: TextStyle(
                                                                                                              fontFamily: 'Poppins',
                                                                                                              fontSize: 12,
                                                                                                              color: Color(0XFF71727A),
                                                                                                            ),
                                                                                                            textAlign: TextAlign.center,
                                                                                                          ),
                                                                                                        ),
                                                                                                        // ignore: sized_box_for_whitespace
                                                                                                        Container(
                                                                                                          width: 400,
                                                                                                          child: ElevatedButton(
                                                                                                            onPressed: () {
                                                                                                              Navigator.of(context).push(
                                                                                                                MaterialPageRoute(
                                                                                                                  builder: (context) {
                                                                                                                    return const chat();
                                                                                                                  },
                                                                                                                ),
                                                                                                              );
                                                                                                            },
                                                                                                            style: ElevatedButton.styleFrom(
                                                                                                              // ignore: deprecated_member_use
                                                                                                              primary: const Color(0XFF00AEFF),
                                                                                                            ),
                                                                                                            child: const Text(
                                                                                                              'Oke',
                                                                                                              style: TextStyle(
                                                                                                                fontFamily: 'Poppins',
                                                                                                                fontSize: 14,
                                                                                                                fontWeight: FontWeight.w600,
                                                                                                                color: Colors.white,
                                                                                                              ),
                                                                                                            ),
                                                                                                          ),
                                                                                                        ),
                                                                                                      ])))))),
                                                                                )
                                                                              ]));
                                                                        });
                                                                  },
                                                                  style: ElevatedButton
                                                                      .styleFrom(
                                                                    backgroundColor:
                                                                        const Color(
                                                                            0XFF00AEFF),
                                                                  ),
                                                                  child:
                                                                      const Text(
                                                                    'Iya',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white),
                                                                  ),
                                                                )),
                                                            ElevatedButton(
                                                              onPressed: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .push(
                                                                  MaterialPageRoute(
                                                                    builder:
                                                                        (context) {
                                                                      return const kategori();
                                                                    },
                                                                  ),
                                                                );
                                                              },
                                                              style:
                                                                  ElevatedButton
                                                                      .styleFrom(
                                                                backgroundColor:
                                                                    const Color(
                                                                        0XFFF5F8FA),
                                                              ),
                                                              // style:  ButtonStyle(
                                                              //   shape:
                                                              //       MaterialStateProperty
                                                              //           .all(
                                                              //     RoundedRectangleBorder(
                                                              //       borderRadius:
                                                              //           BorderRadius
                                                              //               .circular(
                                                              //                   10),
                                                              //     ),
                                                              //   ),
                                                              // ),
                                                              child: const Text(
                                                                'Batalkan',
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily:
                                                                      'Poppins',
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  color: Color(
                                                                      0XFF9498AB),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              });
                        }
                      },
                      child: const Text("Simpan"),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0XFF00AEFF),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 100, vertical: 16),
                      ),
                    )),
              ],
            )
          ]),
    );
  }
}

class CustomRectTween extends RectTween {
  CustomRectTween({
    @required Rect? begin,
    @required Rect? end,
  }) : super(begin: begin, end: end);
}
