import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:sciptchat/edit2.dart';
import 'package:sciptchat/hasilscript.dart';
// ignore: unused_import
import 'package:sciptchat/scriptchat.dart';

void main() => runApp(const editS());

// ignore: camel_case_types
class editS extends StatelessWidget {
  const editS({Key? key}) : super(key: key);

  @override
  // public void onBackPressed() {}
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final value = await showDialog<bool>(
            context: context,
            builder: (context) {
              return Container(
                  color: Colors.transparent,
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 8.0,
                        left: 8.0,
                        top: 240.0,
                      ),
                      child: AlertDialog(
                        title: const Text(
                          'Delete',
                          style: TextStyle(
                            fontSize: 20,
                            color: Color(0XFF1F2024),
                            fontWeight: FontWeight.w700,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        content: const Text(
                          'Perubahan belum disimpan, apakah anda yakin keluar dari halaman ini?',
                          style:
                              TextStyle(fontSize: 12, color: Color(0XFF71727A)),
                          textAlign: TextAlign.center,
                        ),
                        actions: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: const Color(0XFF00AEFF),
                                    borderRadius: BorderRadius.circular(12),
                                  ),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) {
                                            return const hasil();
                                          },
                                        ),
                                      );
                                    },
                                    child: const Text("Ya, Yakin",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12,
                                        )),
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    border: Border.all(
                                        width: 1.0,
                                        color: const Color(0XFF00AEFF)),
                                    // color: Color(0XFF00AEFF),
                                  ),
                                  child: TextButton(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: (context) {
                                              return const editS();
                                            },
                                          ),
                                        );
                                      },
                                      child: const Text("Tidak",
                                          style: TextStyle(
                                            color: Color(0XFF00AEFF),
                                            fontSize: 12,
                                          ))),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ]));
            });
        if (value != null) {
          return Future.value(value);
        } else {
          return Future.value(false);
        }
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title:
              const Text('Edit Script', style: TextStyle(color: Colors.black)),
          centerTitle: true,
          iconTheme: const IconThemeData(
            color: Color(0XFF00AEFF), //change your color here
          ),
        ),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: ListView(
            padding: const EdgeInsets.only(right: 16, left: 16, top: 12),
            children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Column(children: [
              // ignore: avoid_unnecessary_containers
              Container(
                  child: const Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Script*',
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ))),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
              ),
              SizedBox(
                height: 500,
                child: TextField(
                    decoration: InputDecoration(
                        contentPadding:
                            const EdgeInsets.symmetric(vertical: 60.0),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ))),
              ),
            ]),
          ),
          Container(
              padding: const EdgeInsets.all(20),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return const hasil();
                      },
                    ),
                  );
                },

                // Navigator.of(context).push(
                //   MaterialPageRoute(
                //     builder: (context) {
                //       return hasil();
                //     },
                //   ),
                // );

                child: const Text("Selanjutnya"),
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0XFF00AEFF),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 100, vertical: 15),
                ),
              )),
        ]));
  }
}
