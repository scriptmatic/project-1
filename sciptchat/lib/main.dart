import 'package:flutter/material.dart';
import 'package:sciptchat/scriptchat.dart';
// ignore: unused_import
import 'package:sciptchat/kategori.dart';
// ignore: unused_import
import 'package:sciptchat/edit.dart';
// ignore: unused_import
import 'package:sciptchat/editscript.dart';
// ignore: unused_import
import 'package:sciptchat/hasilscript.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Script Chat',
      home: chat(),
    );
  }
}
