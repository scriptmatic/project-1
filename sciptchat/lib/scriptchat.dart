// ignore_for_file: unused_import, sized_box_for_whitespace, duplicate_ignore, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:sciptchat/kategori.dart';
import 'package:sciptchat/edit.dart';
import 'package:sciptchat/hasilscript.dart';
import 'package:sciptchat/edit2.dart';

void main() => runApp(const chat());

// ignore: camel_case_types
class chat extends StatelessWidget {
  const chat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title:
              const Text('Buat Daftar', style: TextStyle(color: Colors.black)),
          centerTitle: true,
          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
        ),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: ListView(
            padding: const EdgeInsets.only(right: 16, left: 16, top: 4),
            children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 2.0),
                  // ignore: sized_box_for_whitespace
                  child: Container(
                    width: 40,
                    // color: Colors.amber,
                    child: Image.asset(
                      "assets/Group165.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 3.0,
                  ),
                ),
                Expanded(
                    child: Container(
                        decoration: BoxDecoration(
                          color: const Color(0XFFF8F9FE),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        height: 40,
                        child: const TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            // OutlineInputBorder(
                            //     borderRadius:
                            //         BorderRadius.all(Radius.circular(20.0))),
                            // fillColor: Color(0XFF8F9098),
                            prefixIcon: Icon(Icons.search),
                            contentPadding: EdgeInsets.only(
                                left: 15, bottom: 14, top: 11, right: 15),
                            hintText: 'Search',
                          ),

                          // Image.asset("assets/"),
                        ))),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 5.0, vertical: 8.0),
                        child: Column(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 190,
                              // color: Colors.red,
                              child: const Text(
                                  // child: Text(
                                  "Daftar Script",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                            ),
                            const SizedBox(height: 8),
                            Container(
                                width: 190,
                                // color: Colors.red,
                                child: const Text(
                                    "Kumpulan daftar script yang dibuat",
                                    style: TextStyle(fontSize: 12))),
                          ],
                        )),
                    const SizedBox(
                        // height: 8,
                        width: 0),
                    Align(
                        alignment: Alignment.centerRight,
                        // Padding(
                        // padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Container(
                          // color: Colors.red,
                          padding: const EdgeInsets.all(2),
                          child: ElevatedButton(
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return const kategori();
                                    },
                                  ),
                                );
                              },
                              child: const Text("Tambah Script",
                                  style: TextStyle(fontSize: 14)),
                              style: ElevatedButton.styleFrom(
                                backgroundColor: const Color(0XFF00AEFF),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                // ),
                              )),
                        ))
                  ])),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Column(children: [
                          Container(
                              child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const hasil();
                                  },
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0),
                                  child: Container(
                                      child: Image.asset(
                                          "assets/Rectangle107.png")),
                                ),
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 40),
                                    child: Column(
                                      children: [
                                        Container(
                                            child: const Text(
                                          "Michael's Fashion",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        )),
                                        // ),
                                        Container(
                                          // color: Colors.red,
                                          width: 110,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8,
                                                bottom: 8,
                                                right: 28.0,
                                                left: 0),
                                            child: Container(
                                              width: 30,
                                              height: 32,
                                              decoration: BoxDecoration(
                                                color: const Color(0XFFFFF4DE),
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              ),
                                              child: const Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12.0,
                                                    vertical: 8.0),
                                                child: Center(
                                                  child: Text(
                                                    "Fashion",
                                                    style: TextStyle(
                                                      color: Color(0XFFFFA800),
                                                      fontSize: 12,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    //   ],
                                    // ),
                                  ),
                                ),
                                Container(
                                    height: 70,
                                    // color: Colors.amber,
                                    // child: Padding(
                                    //     padding: EdgeInsets.all(
                                    //         MediaQuery.of(context).size.width /
                                    //             54),
                                    child: Container(
                                      height: 130,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 40.0),
                                        child: PopupMenuButton(
                                          icon: const Icon(
                                            Icons.more_horiz,
                                            color: Color.fromARGB(
                                                255, 110, 110, 110),
                                          ),
                                          itemBuilder: (context) {
                                            return [
                                              PopupMenuItem(
                                                child: Column(children: [
                                                  Container(
                                                    // color: Colors.amber,
                                                    // child: Container(
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        Navigator.of(context)
                                                            .push(
                                                          MaterialPageRoute(
                                                            builder: (context) {
                                                              return const edit2();
                                                            },
                                                          ),
                                                        );
                                                      },
                                                      child: Container(
                                                        width: 100,
                                                        height: 30,
                                                        child: const Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 8.0,
                                                                  bottom: 4.0),
                                                          child: Text(
                                                            "Edit Script",
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                            ),

                                                            // color: Colors.black,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  // ),
                                                  const SizedBox(height: 5),
                                                  Container(
                                                    // color: Colors.amber,
                                                    child: Container(
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) {
                                                                return Container(
                                                                    color: Colors
                                                                        .transparent,
                                                                    child: Column(
                                                                        children: [
                                                                          Padding(
                                                                            padding:
                                                                                const EdgeInsets.only(
                                                                              right: 14.0,
                                                                              left: 14.0,
                                                                              top: 240.0,
                                                                            ),
                                                                            child:
                                                                                AlertDialog(
                                                                              title: const Text(
                                                                                'Delete',
                                                                                style: TextStyle(
                                                                                  fontSize: 20,
                                                                                  color: Color(0XFF1F2024),
                                                                                  fontWeight: FontWeight.w700,
                                                                                ),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              content: const Text(
                                                                                'Apakah anda yakin ingin menghapus Script ini?',
                                                                                style: TextStyle(fontSize: 12, color: Color(0XFF71727A)),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              actions: [
                                                                                Padding(
                                                                                  padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                    children: [
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          color: const Color(0XFF00AEFF),
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                          onPressed: () {
                                                                                            Navigator.of(context).push(
                                                                                              MaterialPageRoute(
                                                                                                builder: (context) {
                                                                                                  return const chat();
                                                                                                },
                                                                                              ),
                                                                                            );
                                                                                          },
                                                                                          child: const Text("Ya, Yakin",
                                                                                              style: TextStyle(
                                                                                                color: Colors.white,
                                                                                                fontSize: 12,
                                                                                              )),
                                                                                        ),
                                                                                      ),
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                          border: Border.all(width: 1.0, color: const Color(0XFF00AEFF)),
                                                                                          // color: Color(0XFF00AEFF),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                            onPressed: () {
                                                                                              Navigator.of(context).push(
                                                                                                MaterialPageRoute(
                                                                                                  builder: (context) {
                                                                                                    return const chat();
                                                                                                  },
                                                                                                ),
                                                                                              );
                                                                                            },
                                                                                            child: const Text("Tidak",
                                                                                                style: TextStyle(
                                                                                                  color: Color(0XFF00AEFF),
                                                                                                  fontSize: 12,
                                                                                                ))),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )
                                                                        ]));
                                                              });
                                                        },
                                                        child: Container(
                                                          width: 100,
                                                          child: const Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    bottom: 8.0,
                                                                    top: 4.0),
                                                            child: Text(
                                                              "Delete Script",
                                                              style: TextStyle(
                                                                fontSize: 14,
                                                              ),
                                                              // color: Colors.black,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ]),
                                              ),
                                            ];
                                          },
                                        ),
                                      ),
                                    )
                                    // )
                                    ),
                              ],
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromARGB(255, 255, 255, 255),
                              padding: const EdgeInsets.symmetric(vertical: 30),
                            ),
                          ))
                        ])),
                  ])),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Column(children: [
                          Container(
                              child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const hasil();
                                  },
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0),
                                  child: Container(
                                      child: Image.asset(
                                          "assets/Rectangle107.png")),
                                ),
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 40),
                                    child: Column(
                                      children: [
                                        Container(
                                            child: const Text(
                                          "Michael's Fashion",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        )),
                                        // ),
                                        Container(
                                          // color: Colors.red,
                                          width: 110,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8,
                                                bottom: 8,
                                                right: 28.0,
                                                left: 0),
                                            child: Container(
                                              width: 30,
                                              height: 32,
                                              decoration: BoxDecoration(
                                                color: const Color(0XFFFFF4DE),
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              ),
                                              child: const Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12.0,
                                                    vertical: 8.0),
                                                child: Center(
                                                  child: Text(
                                                    "Fashion",
                                                    style: TextStyle(
                                                      color: Color(0XFFFFA800),
                                                      fontSize: 12,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    //   ],
                                    // ),
                                  ),
                                ),
                                Container(
                                    height: 70,
                                    // color: Colors.amber,
                                    // child: Padding(
                                    //     padding: EdgeInsets.all(
                                    //         MediaQuery.of(context).size.width /
                                    //             54),
                                    child: Container(
                                      height: 105,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 40.0),
                                        child: PopupMenuButton(
                                          icon: const Icon(
                                            Icons.more_horiz,
                                            color: Color.fromARGB(
                                                255, 110, 110, 110),
                                          ),
                                          itemBuilder: (context) {
                                            return [
                                              PopupMenuItem(
                                                child: Column(children: [
                                                  Container(
                                                    // color: Colors.amber,
                                                    // child: Container(
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        Navigator.of(context)
                                                            .push(
                                                          MaterialPageRoute(
                                                            builder: (context) {
                                                              return const edit2();
                                                            },
                                                          ),
                                                        );
                                                      },
                                                      child: Container(
                                                        width: 100,
                                                        height: 30,
                                                        child: const Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 8.0,
                                                                  bottom: 4.0),
                                                          child: Text(
                                                            "Edit Script",
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                            ),

                                                            // color: Colors.black,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  // ),
                                                  const SizedBox(height: 5),
                                                  Container(
                                                    color: Colors.transparent,
                                                    child: Container(
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) {
                                                                return Container(
                                                                    color: Colors
                                                                        .transparent,
                                                                    child: Column(
                                                                        children: [
                                                                          Padding(
                                                                            padding:
                                                                                const EdgeInsets.only(
                                                                              right: 14.0,
                                                                              left: 14.0,
                                                                              top: 240.0,
                                                                            ),
                                                                            child:
                                                                                AlertDialog(
                                                                              title: const Text(
                                                                                'Delete',
                                                                                style: TextStyle(
                                                                                  fontSize: 20,
                                                                                  color: Color(0XFF1F2024),
                                                                                  fontWeight: FontWeight.w700,
                                                                                ),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              content: const Text(
                                                                                'Apakah anda yakin ingin menghapus Script ini?',
                                                                                style: TextStyle(fontSize: 12, color: Color(0XFF71727A)),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              actions: [
                                                                                Padding(
                                                                                  padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                    children: [
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          color: const Color(0XFF00AEFF),
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                          onPressed: () {
                                                                                            Navigator.of(context).push(
                                                                                              MaterialPageRoute(
                                                                                                builder: (context) {
                                                                                                  return const chat();
                                                                                                },
                                                                                              ),
                                                                                            );
                                                                                          },
                                                                                          child: const Text("Ya, Yakin",
                                                                                              style: TextStyle(
                                                                                                color: Colors.white,
                                                                                                fontSize: 12,
                                                                                              )),
                                                                                        ),
                                                                                      ),
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                          border: Border.all(width: 1.0, color: const Color(0XFF00AEFF)),
                                                                                          // color: Color(0XFF00AEFF),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                            onPressed: () {
                                                                                              Navigator.of(context).push(
                                                                                                MaterialPageRoute(
                                                                                                  builder: (context) {
                                                                                                    return const chat();
                                                                                                  },
                                                                                                ),
                                                                                              );
                                                                                            },
                                                                                            child: const Text("Tidak",
                                                                                                style: TextStyle(
                                                                                                  color: Color(0XFF00AEFF),
                                                                                                  fontSize: 12,
                                                                                                ))),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )
                                                                        ]));
                                                              });
                                                        },
                                                        child: Container(
                                                          width: 100,
                                                          child: const Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    bottom: 8.0,
                                                                    top: 4.0),
                                                            child: Text(
                                                              "Delete Script",
                                                              style: TextStyle(
                                                                fontSize: 14,
                                                              ),
                                                              // color: Colors.black,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ]),
                                              ),
                                            ];
                                          },
                                        ),
                                      ),
                                    )
                                    // )
                                    ),
                              ],
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromARGB(255, 255, 255, 255),
                              padding: const EdgeInsets.symmetric(vertical: 30),
                            ),
                          ))
                        ])),
                  ])),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Column(children: [
                          Container(
                              child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const hasil();
                                  },
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0),
                                  child: Container(
                                      child: Image.asset(
                                          "assets/Rectangle107.png")),
                                ),
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 40),
                                    child: Column(
                                      children: [
                                        Container(
                                            child: const Text(
                                          "Michael's Fashion",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        )),
                                        // ),
                                        Container(
                                          // color: Colors.red,
                                          width: 110,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8,
                                                bottom: 8,
                                                right: 28.0,
                                                left: 0),
                                            child: Container(
                                              width: 30,
                                              height: 32,
                                              decoration: BoxDecoration(
                                                color: const Color(0XFFFFF4DE),
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              ),
                                              child: const Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12.0,
                                                    vertical: 8.0),
                                                child: Center(
                                                  child: Text(
                                                    "Fashion",
                                                    style: TextStyle(
                                                      color: Color(0XFFFFA800),
                                                      fontSize: 12,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    //   ],
                                    // ),
                                  ),
                                ),
                                Container(
                                    height: 70,
                                    // color: Colors.amber,
                                    // child: Padding(
                                    //     padding: EdgeInsets.all(
                                    //         MediaQuery.of(context).size.width /
                                    //             54),
                                    child: Container(
                                      // color: Colors.red,
                                      height: 130,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 40.0),
                                        child: PopupMenuButton(
                                          icon: const Icon(
                                            Icons.more_horiz,
                                            color: Color.fromARGB(
                                                255, 110, 110, 110),
                                          ),
                                          itemBuilder: (context) {
                                            return [
                                              PopupMenuItem(
                                                child: Column(children: [
                                                  Container(
                                                    // color: Colors.amber,
                                                    // child: Container(
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        Navigator.of(context)
                                                            .push(
                                                          MaterialPageRoute(
                                                            builder: (context) {
                                                              return const edit2();
                                                            },
                                                          ),
                                                        );
                                                      },
                                                      child: Container(
                                                        width: 100,
                                                        height: 30,
                                                        child: const Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 8.0,
                                                                  bottom: 4.0),
                                                          child: Text(
                                                            "Edit Script",
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                            ),

                                                            // color: Colors.black,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  // ),
                                                  const SizedBox(height: 5),
                                                  Container(
                                                    // color: Colors.amber,
                                                    child: Container(
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) {
                                                                return Container(
                                                                    color: Colors
                                                                        .transparent,
                                                                    child: Column(
                                                                        children: [
                                                                          Padding(
                                                                            padding:
                                                                                const EdgeInsets.only(
                                                                              right: 14.0,
                                                                              left: 14.0,
                                                                              top: 240.0,
                                                                            ),
                                                                            child:
                                                                                AlertDialog(
                                                                              title: const Text(
                                                                                'Delete',
                                                                                style: TextStyle(
                                                                                  fontSize: 20,
                                                                                  color: Color(0XFF1F2024),
                                                                                  fontWeight: FontWeight.w700,
                                                                                ),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              content: const Text(
                                                                                'Apakah anda yakin ingin menghapus Script ini?',
                                                                                style: TextStyle(fontSize: 12, color: Color(0XFF71727A)),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              actions: [
                                                                                Padding(
                                                                                  padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                    children: [
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          color: const Color(0XFF00AEFF),
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                          onPressed: () {
                                                                                            Navigator.of(context).push(
                                                                                              MaterialPageRoute(
                                                                                                builder: (context) {
                                                                                                  return const chat();
                                                                                                },
                                                                                              ),
                                                                                            );
                                                                                          },
                                                                                          child: const Text("Ya, Yakin",
                                                                                              style: TextStyle(
                                                                                                color: Colors.white,
                                                                                                fontSize: 12,
                                                                                              )),
                                                                                        ),
                                                                                      ),
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                          border: Border.all(width: 1.0, color: const Color(0XFF00AEFF)),
                                                                                          // color: Color(0XFF00AEFF),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                            onPressed: () {
                                                                                              Navigator.of(context).push(
                                                                                                MaterialPageRoute(
                                                                                                  builder: (context) {
                                                                                                    return const chat();
                                                                                                  },
                                                                                                ),
                                                                                              );
                                                                                            },
                                                                                            child: const Text("Tidak",
                                                                                                style: TextStyle(
                                                                                                  color: Color(0XFF00AEFF),
                                                                                                  fontSize: 12,
                                                                                                ))),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )
                                                                        ]));
                                                              });
                                                        },
                                                        child: Container(
                                                          width: 100,
                                                          child: const Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    bottom: 8.0,
                                                                    top: 4.0),
                                                            child: Text(
                                                              "Delete Script",
                                                              style: TextStyle(
                                                                fontSize: 14,
                                                              ),
                                                              // color: Colors.black,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ]),
                                              ),
                                            ];
                                          },
                                        ),
                                      ),
                                    )
                                    // )
                                    ),
                              ],
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromARGB(255, 255, 255, 255),
                              padding: const EdgeInsets.symmetric(vertical: 30),
                            ),
                          ))
                        ])),
                  ])),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Column(children: [
                          Container(
                              child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const hasil();
                                  },
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0),
                                  child: Container(
                                      child: Image.asset(
                                          "assets/Rectangle107.png")),
                                ),
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 40),
                                    child: Column(
                                      children: [
                                        Container(
                                            child: const Text(
                                          "Michael's Fashion",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        )),
                                        // ),
                                        Container(
                                          // color: Colors.red,
                                          width: 110,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8,
                                                bottom: 8,
                                                right: 28.0,
                                                left: 0),
                                            child: Container(
                                              width: 30,
                                              height: 32,
                                              decoration: BoxDecoration(
                                                color: const Color(0XFFFFF4DE),
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              ),
                                              child: const Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12.0,
                                                    vertical: 8.0),
                                                child: Center(
                                                  child: Text(
                                                    "Fashion",
                                                    style: TextStyle(
                                                      color: Color(0XFFFFA800),
                                                      fontSize: 12,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    //   ],
                                    // ),
                                  ),
                                ),
                                Container(
                                    height: 70,
                                    // color: Colors.amber,
                                    // child: Padding(
                                    //     padding: EdgeInsets.all(
                                    //         MediaQuery.of(context).size.width /
                                    //             54),
                                    child: Container(
                                      height: 130,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 40.0),
                                        child: PopupMenuButton(
                                          icon: const Icon(
                                            Icons.more_horiz,
                                            color: Color.fromARGB(
                                                255, 110, 110, 110),
                                          ),
                                          itemBuilder: (context) {
                                            return [
                                              PopupMenuItem(
                                                child: Column(children: [
                                                  Container(
                                                    // color: Colors.amber,
                                                    // child: Container(
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        Navigator.of(context)
                                                            .push(
                                                          MaterialPageRoute(
                                                            builder: (context) {
                                                              return const edit2();
                                                            },
                                                          ),
                                                        );
                                                      },
                                                      child: Container(
                                                        width: 100,
                                                        height: 30,
                                                        child: const Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 8.0,
                                                                  bottom: 4.0),
                                                          child: Text(
                                                            "Edit Script",
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                            ),

                                                            // color: Colors.black,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  // ),
                                                  const SizedBox(height: 5),
                                                  Container(
                                                    // color: Colors.amber,
                                                    child: Container(
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) {
                                                                return Container(
                                                                    color: Colors
                                                                        .transparent,
                                                                    child: Column(
                                                                        children: [
                                                                          Padding(
                                                                            padding:
                                                                                const EdgeInsets.only(
                                                                              right: 14.0,
                                                                              left: 14.0,
                                                                              top: 240.0,
                                                                            ),
                                                                            child:
                                                                                AlertDialog(
                                                                              title: const Text(
                                                                                'Delete',
                                                                                style: TextStyle(
                                                                                  fontSize: 20,
                                                                                  color: Color(0XFF1F2024),
                                                                                  fontWeight: FontWeight.w700,
                                                                                ),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              content: const Text(
                                                                                'Apakah anda yakin ingin menghapus Script ini?',
                                                                                style: TextStyle(fontSize: 12, color: Color(0XFF71727A)),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              actions: [
                                                                                Padding(
                                                                                  padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                    children: [
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          color: const Color(0XFF00AEFF),
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                          onPressed: () {
                                                                                            Navigator.of(context).push(
                                                                                              MaterialPageRoute(
                                                                                                builder: (context) {
                                                                                                  return const chat();
                                                                                                },
                                                                                              ),
                                                                                            );
                                                                                          },
                                                                                          child: const Text("Ya, Yakin",
                                                                                              style: TextStyle(
                                                                                                color: Colors.white,
                                                                                                fontSize: 12,
                                                                                              )),
                                                                                        ),
                                                                                      ),
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                          border: Border.all(width: 1.0, color: const Color(0XFF00AEFF)),
                                                                                          // color: Color(0XFF00AEFF),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                            onPressed: () {
                                                                                              Navigator.of(context).push(
                                                                                                MaterialPageRoute(
                                                                                                  builder: (context) {
                                                                                                    return const chat();
                                                                                                  },
                                                                                                ),
                                                                                              );
                                                                                            },
                                                                                            child: const Text("Tidak",
                                                                                                style: TextStyle(
                                                                                                  color: Color(0XFF00AEFF),
                                                                                                  fontSize: 12,
                                                                                                ))),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )
                                                                        ]));
                                                              });
                                                        },
                                                        child: Container(
                                                          width: 100,
                                                          child: const Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    bottom: 8.0,
                                                                    top: 4.0),
                                                            child: Text(
                                                              "Delete Script",
                                                              style: TextStyle(
                                                                fontSize: 14,
                                                              ),
                                                              // color: Colors.black,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ]),
                                              ),
                                            ];
                                          },
                                        ),
                                      ),
                                    )
                                    // )
                                    ),
                              ],
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromARGB(255, 255, 255, 255),
                              padding: const EdgeInsets.symmetric(vertical: 30),
                            ),
                          ))
                        ])),
                  ])),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: Column(children: [
                          Container(
                              child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) {
                                    return const hasil();
                                  },
                                ),
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0),
                                  child: Container(
                                      child: Image.asset(
                                          "assets/Rectangle107.png")),
                                ),
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 40),
                                    child: Column(
                                      children: [
                                        Container(
                                            child: const Text(
                                          "Michael's Fashion",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        )),
                                        // ),
                                        Container(
                                          // color: Colors.red,
                                          width: 110,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8,
                                                bottom: 8,
                                                right: 28.0,
                                                left: 0),
                                            child: Container(
                                              width: 30,
                                              height: 32,
                                              decoration: BoxDecoration(
                                                color: const Color(0XFFFFF4DE),
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              ),
                                              child: const Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 12.0,
                                                    vertical: 8.0),
                                                child: Center(
                                                  child: Text(
                                                    "Fashion",
                                                    style: TextStyle(
                                                      color: Color(0XFFFFA800),
                                                      fontSize: 12,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    //   ],
                                    // ),
                                  ),
                                ),
                                Container(
                                    height: 70,
                                    // color: Colors.amber,
                                    // child: Padding(
                                    //     padding: EdgeInsets.all(
                                    //         MediaQuery.of(context).size.width /
                                    //             54),
                                    child: Container(
                                      height: 130,
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 40.0),
                                        child: PopupMenuButton(
                                          icon: const Icon(
                                            Icons.more_horiz,
                                            color: Color.fromARGB(
                                                255, 110, 110, 110),
                                          ),
                                          itemBuilder: (context) {
                                            return [
                                              PopupMenuItem(
                                                child: Column(children: [
                                                  Container(
                                                    // color: Colors.amber,
                                                    // child: Container(
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        Navigator.of(context)
                                                            .push(
                                                          MaterialPageRoute(
                                                            builder: (context) {
                                                              return const edit2();
                                                            },
                                                          ),
                                                        );
                                                      },
                                                      child: Container(
                                                        width: 100,
                                                        height: 30,
                                                        child: const Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  top: 8.0,
                                                                  bottom: 4.0),
                                                          child: Text(
                                                            "Edit Script",
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                            ),

                                                            // color: Colors.black,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  // ),
                                                  const SizedBox(height: 5),
                                                  Container(
                                                    // color: Colors.amber,
                                                    child: Container(
                                                      child: GestureDetector(
                                                        onTap: () {
                                                          showDialog(
                                                              context: context,
                                                              builder:
                                                                  (context) {
                                                                return Container(
                                                                    color: Colors
                                                                        .transparent,
                                                                    child: Column(
                                                                        children: [
                                                                          Padding(
                                                                            padding:
                                                                                const EdgeInsets.only(
                                                                              right: 14.0,
                                                                              left: 14.0,
                                                                              top: 240.0,
                                                                            ),
                                                                            child:
                                                                                AlertDialog(
                                                                              title: const Text(
                                                                                'Delete',
                                                                                style: TextStyle(
                                                                                  fontSize: 20,
                                                                                  color: Color(0XFF1F2024),
                                                                                  fontWeight: FontWeight.w700,
                                                                                ),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              content: const Text(
                                                                                'Apakah anda yakin ingin menghapus Script ini?',
                                                                                style: TextStyle(fontSize: 12, color: Color(0XFF71727A)),
                                                                                textAlign: TextAlign.center,
                                                                              ),
                                                                              actions: [
                                                                                Padding(
                                                                                  padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                    children: [
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          color: const Color(0XFF00AEFF),
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                          onPressed: () {
                                                                                            Navigator.of(context).push(
                                                                                              MaterialPageRoute(
                                                                                                builder: (context) {
                                                                                                  return const chat();
                                                                                                },
                                                                                              ),
                                                                                            );
                                                                                          },
                                                                                          child: const Text("Ya, Yakin",
                                                                                              style: TextStyle(
                                                                                                color: Colors.white,
                                                                                                fontSize: 12,
                                                                                              )),
                                                                                        ),
                                                                                      ),
                                                                                      Container(
                                                                                        decoration: BoxDecoration(
                                                                                          borderRadius: BorderRadius.circular(12),
                                                                                          border: Border.all(width: 1.0, color: const Color(0XFF00AEFF)),
                                                                                          // color: Color(0XFF00AEFF),
                                                                                        ),
                                                                                        child: TextButton(
                                                                                            onPressed: () {
                                                                                              Navigator.of(context).push(
                                                                                                MaterialPageRoute(
                                                                                                  builder: (context) {
                                                                                                    return const chat();
                                                                                                  },
                                                                                                ),
                                                                                              );
                                                                                            },
                                                                                            child: const Text("Tidak",
                                                                                                style: TextStyle(
                                                                                                  color: Color(0XFF00AEFF),
                                                                                                  fontSize: 12,
                                                                                                ))),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )
                                                                        ]));
                                                              });
                                                        },
                                                        child: Container(
                                                          width: 100,
                                                          child: const Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    bottom: 8.0,
                                                                    top: 4.0),
                                                            child: Text(
                                                              "Delete Script",
                                                              style: TextStyle(
                                                                fontSize: 14,
                                                              ),
                                                              // color: Colors.black,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ]),
                                              ),
                                            ];
                                          },
                                        ),
                                      ),
                                    )
                                    // )
                                    ),
                              ],
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromARGB(255, 255, 255, 255),
                              padding: const EdgeInsets.symmetric(vertical: 30),
                            ),
                          ))
                        ])),
                  ])),
        ]));
  }
}
