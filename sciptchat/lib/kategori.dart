import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:sciptchat/scriptchat.dart';
import 'package:sciptchat/edit.dart';

void main() => runApp(const kategori());

// ignore: camel_case_types
class kategori extends StatelessWidget {
  const kategori({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title:
            const Text('Tambah Script', style: TextStyle(color: Colors.black)),
        centerTitle: true,
        iconTheme: const IconThemeData(
          color: Color(0XFF00AEFF), //change your color here
        ),
      ),
      body: const MyStatefulWidget(),
    );
  }
  // home: Scaffold(
  //   appBar: AppBar(
  //     backgroundColor: Colors.white,
  //     title: Text('Tambah Script', style: TextStyle(color: Colors.black)),
  //     centerTitle: true,
  //     iconTheme: IconThemeData(
  //       color: Color(0XFF00AEFF), //change your color here
  //     ),
  //   ),
  //   body: const MyStatefulWidget(),
  // ),
  // );
}
// }

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  // Initial Selected Value
  String dropdownvalue = ' Fashion Pria';

  // List of items in our dropdown menu
  var items = [
    ' Fashion Pria',
    ' Fashion Wanita',
    ' Makanan',
    ' Peralatan Rumah Tangga',
    ' Fashion Muslim',
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ListView(
        padding: const EdgeInsets.only(right: 16, left: 16, top: 12),
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 15),
                  width: 85,
                  height: 67,
                  // color: Colors.amber,
                  child: Row(children: [
                    GestureDetector(
                        onTap: () {},
                        child: Image.asset(
                          'assets/Active.png',
                          fit: BoxFit.cover,
                        ))
                  ]),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 15),
                  width: 25,
                  height: 67,
                  // color: Colors.amber,
                  child: Row(children: [
                    Image.asset(
                      'assets/Arrow.png',
                      fit: BoxFit.cover,
                    )
                  ]),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 15),
                  width: 100,
                  height: 67,
                  // color: Colors.amber,
                  child: Row(children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return const edit();
                            },
                          ),
                        );
                      },
                      child: Image.asset(
                        'assets/Step5(2).png',
                        // width: 85,
                        // height: 65,
                        fit: BoxFit.cover,
                      ),
                    )
                  ]),
                ),
              ],
            ),
          ),
          const SizedBox(height: 25),
          Container(
              padding: const EdgeInsets.only(top: 10, left: 10),
              child: const Text(
                "Kategori",
                textAlign: TextAlign.start,
              )),
          const SizedBox(height: 15),
          // ignore: sized_box_for_whitespace
          Container(
            // color: Colors.amber,
            width: 100,
            child: Column(
              children: [
                DropdownButton(
                  // Initial Value
                  isExpanded: true,
                  value: dropdownvalue,

                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: items.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Text(
                        items,
                        textAlign: TextAlign.left,
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (String? newValue) {
                    setState(() {
                      dropdownvalue = newValue!;
                    });
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 375),
          Container(
              padding: const EdgeInsets.all(20),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) {
                        return const edit();
                      },
                    ),
                  );
                },
                child: const Text("Selanjutnya"),
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0XFF00AEFF),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 100, vertical: 15),
                ),
              )),
        ],
      ),
    );
  }
}
